<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/icon.png" type="image/png">
        <title>Un Millón De Arboles</title>
        <!-- main css -->
        
         <link rel="stylesheet" href="/css/registros.css">
        
        <link rel="stylesheet" href="css/fontello.css">
         <!-- Bootstrap CSS -->
        
             <link rel="stylesheet" href="bootstraps/css/bootstrap.css">
        
        
    </head>
    <body>
        
        <!--================Header Menu Area=================--> 
     
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="">
          <a class="navbar-brand" href="index.html" >Home</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="login.php">Login <span class="sr-only">(current)</span></a>
              </li>
            
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Redes Sociales
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Facebook</a>
                  <a class="dropdown-item" href="#">Instagram</a>
                  <a class="dropdown-item" href="#">Twitter</a>
                </div>
              </li>
            </ul>
          </div>
            <img class="icon" src="/img/icon.png" style="height: 80px;width: 80px; margin-right: 100px"/>
        </nav>
     
        
    </header>
        
    <main>
       <section id="banner-re">
          
           <div class="box-re">
             <h1>Registro</h1>
            <form action="logica/regis.php" onsubmit="saveStudent();" method="POST">
                 <input type="number" id="0" name="id" placeholder="Numero de identificación  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Numero de identificación  '" required="" >
                
                <input type="text" id="1" name="nombre1" placeholder="Primer Nombre " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Primer Nombre  '" required="">
                <input type="text" id="2" name="nombre2" placeholder="Apellido  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Apellido  '" required="" >
                <input type="number" id="3" name="tel" placeholder="Telefono  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Telefono   '" required="" >
                <input type="email" id="4" name="correo" placeholder=" Correo Electronico   " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Correo Electronico   '" required="">
                <input type="text" id="5" name="direccion" placeholder="Dirección  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Dirección  '" required="" >
                <input type="text" id="6" name="pais" placeholder="País  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'País  '" required="" >
                
                <input type="password" id="7" name="contra" placeholder="Contraseña  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Contraseña  '" required="" >
                <button  type="submit" onclick="">Registrarse</button>
                <a href="login.php"><u>¿Ya tienes cuenta? Logueate</u></a>
            </form>
             
           </div>
       
       </section>   
        
     
      
        
    </main>
        
    </body>
</html>