<?php
  session_start();
  include("logica/logcliente.php");


  $user = $_SESSION['user'];
  if ($user['usuario'] != "USER") {
      
      header('Location: /login.php');
  }

  ?>


<!doctype html>
<html lang="en">

<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/icon.png" type="image/png">
        <title>Un Millón De Arboles</title>
        <!-- main css -->
 	    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	    <script src="/js/main.js"></script>
        <script src="js/validacion.js"></script>
        
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="css/fontello.css">
 
        <!-- Bootstrap CSS -->
        
        <link rel="stylesheet" href="bootstraps/css/bootstrap.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!--Fontawesome CDN-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        
        <link rel="stylesheet" href="css/cliente.css">
    </head>
    <body>
         
        <!--================Header Menu Area=================--> 
     
 <header>
       
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="">
          <a class="navbar-brand" href="index.html" >Home</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="login.php">Login <span class="sr-only">(current)</span></a>
              </li>
            
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Redes Sociales
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Facebook</a>
                  <a class="dropdown-item" href="#">Instagram</a>
                  <a class="dropdown-item" href="#">Twitter</a>
                </div>
              </li>
            </ul>
          </div>
            <img class="icon" src="/img/icon.png" style="height: 80px;width: 80px; margin-right: 100px"/>
        </nav>
     
        
    </header>
    <main>
         <section id="banner-ad">
           <img class="fondo" src="/img/client.jpg" />
           <div class="box-ad">
                    
                    <img class="user" src="/img/user.jpg" />
                    <p class="name" >Bienvenido:  <?php echo $user['nombre'] ." ". $user['apellido']  ?></p>
               
                   
                    
               
                    <ul class="dashboard">
                        <li><a href="#dash"><span class="tab-text">Dashboard   </span></a></li>
                        <li><a href="#rides"><span class="tab-text">Árboles  </span></a></li>
                        <li><a href="#setting"><span class="tab-text">Ver Fotos </span></a></li>
                    </ul>
                    
               
                </div>
         
           <div class="box-ad2">
          
                <div class="secciones">
                        <article id="dash">
                            
                             <script>
                                        function mostrar(){
                                            var id = document.getElementById('comboEspecies').value;
                                            window.location.href = 'http://localhost/cliente.php?id='+id;
                                        }
                            </script>
                            
                            <form action="logica/logcliente.php" method="post">
                                
                            <select name="combo" id="comboEspecies" onchange="return mostrar();" >
                                 <option value="">Selecione una especie de árbol...</option>
                                 <?php 
                                        
                                        $miarreglo = cargarEspecies();
                                        if(isset($miarreglo)){
                                            foreach($miarreglo as $key => $value) {?>
                                            <option value="<?php echo $value['nombre'];?>"><?php echo $value['nombre'];?></option>
                                            <?php
                                        }
                                        }
                                        
                                ?>
                                   
                            </select> 
                            
                            <?php $value = llenar();?>
                                 
                               
                            <input type="hidden" name="id" id="id" value="<?php $id = idespecie($value); echo $id;?>">
                            <input type="hidden" name="iduser" id="iduser" value="<?php echo $user['id']; ?>">
                                
                            <input type="text" name="nom" id="nom" placeholder=" Nombre " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre '" required="" >
                             
                            <input type="number" id="monto" name="monto" placeholder=" Monto para donar " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Monto para donar  '" required="" > 
                            
                             <button type="submit" id="btnSave">Comprar Árbol</button>
                            </form>
                            
                            <div class="logout">
                                <div class="icon-logout">
                                    <span class="input"><i class="fas fa-sign-out-alt"></i></span>
                                </div>
                                <a href="/logica/logout.php" id="out">Logout</a>
                            </div>
                            
                        </article>
                        <article id="rides">
                            <div class="texto">
                                <h2>Mis Árboles</h2> 
                                <script>
                                        function mostrar2(){
                                            var idarbol = document.getElementById('combo1').value;
                                            window.location.href = 'http://localhost/cliente.php?idarbol='+idarbol;
                                        }
                                 </script> 
                                <form >
                                    
                                <select name="combo" id="combo1" onchange="return mostrar2();" >
                                    <option value="">Selecione el ID del Árbol...</option>
                                    <?php 
                                        $idamigo = $user['id'];
                                        $miarreglo = idArbol($idamigo);
                                        if(isset($miarreglo)){
                                            foreach($miarreglo as $key => $value) {?>
                                            <option value="<?php echo $value['id'];?>"><?php echo $value['id'];?></option>
                                            <?php
                                        }
                                        }
                                        
                                    ?>
                                    
                                   
                                </select>
                                <?php $val = llenar2(); ?>    
                                <?php
                                    
                                     if(isset($val)){
                                     $idamigo = $user['id'];
                                     $cargar = cargarArboles($idamigo,$val);
                                     if(isset($cargar)){
                                        $altura;
                                        $nombre;
                                        $nomEspecie;
                                        $imgone;
                                        $imgtwo;
                                        $imgthree;
                                        foreach($cargar as $key => $value2) {
                                             $altura = $value2['altura'];
                                             $nombre = $value2['nombrearbol'];
                                             $nomEspecie = $value2['nombre'];
                                             $imgone = $value2['one'];
                                             $imgtwo = $value2['two'];
                                             $imgthree = $value2['three'];
                                             
                                        }
                                     }
                                     }

                                ?>
                                    
                                 
                                <input type="text" name="ridename" id="name" placeholder=" Nombre del Árbol " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Nombre del Árbol '" value="<?php if(isset($nombre)){ echo $nombre;} ?>" readonly><br>  
                                
               
                                <input type="text" name="altura" id="altura" placeholder=" Altura " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Altura '" value="<?php if(isset($altura)){ echo $altura;} ?>" readonly>

                                <input type="text" name="especie" id="especie" placeholder=" Especie " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Especie '"  value="<?php if(isset($nomEspecie)){ echo $nomEspecie;} ?>" readonly>
                            
                                </form>
                            </div>
                            
                             
                            
                        </article>
                        <article id="setting">
                            
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
                              <div class="carousel-inner" style="height:220px; width:450px;  margin-left: 28%; margin-top:3.5%;">
                                <div class="carousel-item active" >
                                  <img class="d-block w-100" src="<?php if(isset($imgone)){ echo $imgone;}?>" alt="First slide" style="height:220px; width:450px;">
                                </div>
                                <div class="carousel-item">
                                  <img class="d-block w-100" src="<?php if(isset($imgtwo)){ echo $imgtwo;}?>" alt="Second slide" style="height:220px; width:450px;" >
                                </div>
                                <div class="carousel-item">
                                  <img class="d-block w-100" src="<?php if(isset($imgthree)){ echo $imgthree;}?>" alt="Third slide" style="height:220px; width:450px;">
                                </div>
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div>
                           
                            
                        </article>
                       
                    </div>
           </div>
       </section>   
        
        
    </main>
  
             <!-- jQuery first, then Popper.js, then Bootstrap JS -->
         <script src="bootstraps/js/jquery-3.3.1.min.js"></script>
         <script src="bootstraps/js/bootstrap.min.js"></script>
    
     
        
    
    
    </body>
    
    
</html>