/*Aqui es jQuery para el dashboard la funcion lo que hace es que en la class dashboard en el html a la hora de cliclear
 a la etiqueta a su seccion correspondiente se muestra y las demas se ocultan*/


$(document).ready(function(){
	$('ul.dashboard li a:first').addClass('active');
	$('.secciones article').hide();
	$('.secciones article:first').show();

	$('ul.dashboard li a').click(function(){
		$('ul.dashboard li a').removeClass('active');
		$(this).addClass('active');
		$('.secciones article').hide();

		var activeTab = $(this).attr('href');
		$(activeTab).show();
       
		return false;
	});
});