
<?php
 
  session_start();
  include("logica/logadmin.php");

  $user = $_SESSION['user'];
  if ($user['usuario'] != "ADMIN") {
      
      header('Location: /login.php');
  }

  ?>

<!doctype html>
<html lang="en">

<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/icon.png" type="image/png">
        <title>Un Millón De Arboles</title>
        <!-- main css -->
 	    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	    <script src="/js/mains.js"></script>
        <link rel="stylesheet" href="css/fontello.css">
 
        <!-- Bootstrap CSS -->
        
        <link rel="stylesheet" href="bootstraps/css/bootstrap.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!--Fontawesome CDN-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        
        <link rel="stylesheet" href="css/admistra.css">
    </head>
    <body>
         
        <!--================Header Menu Area=================--> 
     
 <header>
       
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="">
          <a class="navbar-brand" href="index.html" >Home</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="login.php">Login <span class="sr-only">(current)</span></a>
              </li>
            
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Redes Sociales
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Facebook</a>
                  <a class="dropdown-item" href="#">Instagram</a>
                  <a class="dropdown-item" href="#">Twitter</a>
                </div>
              </li>
            </ul>
          </div>
            <img class="icon" src="/img/icon.png" style="height: 80px;width: 80px; margin-right: 100px"/>
        </nav>
     
        
    </header>
    <main>
         <section id="banner-ad">
           <img class="fondo" src="/img/banner.jpg" />
           <div class="box-ad">
                    
                    <img class="user" src="/img/user.jpg" />
               
                    <p class="name" >Bienvenido:  <?php echo $user['nombre'] ." ". $user['apellido']  ?></p>
               
                   
                    
               
                    <ul class="dashboard">
                        <li><a href="#dash"><span class="tab-text">Dashboard   </span></a></li>
                        <li><a href="#rides"><span class="tab-text">Árboles  </span></a></li>
                        <li><a href="#setting"><span class="tab-text">Ver Fotos </span></a></li>
                    </ul>
                    
               
                </div>
         
           <div class="box-ad2">
          
                <div class="secciones">
                        <article id="dash">
                            <div class="input-group form-group">
                                
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                 <p class="name" > Cantidad de amigos registrados: <?php  $resultado = cantidadUsers(); echo $resultado?></p>
                                </div>

                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-tree"></i></span>
                                </div>
                                 <p class="name" > Cantidad de árboles sembrados:  <?php  $resultado = cantidadArboles(); echo $resultado?></p>
                            </div>
                            
                            <div class="logout">
                                <div class="icon-logout">
                                    <span class="input"><i class="fas fa-sign-out-alt"></i></span>
                                </div>
                                 <a href="/logica/logout.php" id="out">Logout</a>
                            </div>
                        </article>
                        <article id="rides">
                            <div class="texto">
                                <h2>Árboles Amigos</h2> 
                                <div class="opc">
                                    <p><input type="hidden" name="edit"  id="edit" onclick="" ></p>
                                    <p><input type="hidden" name="edit" id="crear" checked> </p>
                                   
                                </div>
                                 <script>
                                        function mostrar(){
                                            var id = document.getElementById('id').value;
                                            window.location.href = 'http://localhost/admi.php?id='+id;
                                        }
                                 </script> 
                                  <script>
                                        function mostrarBox(){
                                            var id = document.getElementById('id').value;
                                            var box = document.getElementById('combo1').value;
                                             window.location.href = 'http://localhost/admi.php?box='+box+'&id='+id;
                                            
                                        }
                                 </script>
                                
                                
                                    <?php  $var = llenar();
                                       if(isset($var)){?>
                                        <input type="number" id="id" name="id" value="<?php echo $var;?>" onchange="return mostrar();"/>   
                                     <?php
                                                       
                                       }else{?>
                                         <input type="number" id="id" name="id" placeholder="Numero de identificación  " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Numero de identificación  '" required=""  onchange="return mostrar();">
                                       <?php
                                       }
                                    ?>
                                    
                                    
                                    
                                    <select name="combo1" id="combo1" onchange="return mostrarBox();" >
                                    <option value=""> Elija el nombre de la especie </option>
                                    <?php 
                                    
                                        $miarreglo = cargarUsuarios($var);
                                        if(isset($miarreglo)){
                                            foreach($miarreglo as $key => $value) {?>
                                            <option value="<?php echo $value['especie'];?>"><?php echo $value['especie'];?></option>
                                            <?php
                                        }
                                        }
                                        
                                    ?>
                                    
                                </select> 
                                 
                                <input type="text" name="ridename" id="name" placeholder=" Nombre usuario " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Nombre usuario '" required="" value="<?php if(isset($value['user'])){ echo $value['user'];}?>" readonly><br>
                                
                                <?php  $cargar = llenar2($var);
                                     if(isset($cargar)){
                                        $altura;
                                        $nombre;
                                        $imgone;
                                        $imgtwo;
                                        $imgthree;
                                        $id;
                                        foreach($cargar as $key => $value2) {
                                             $altura = $value2['altura'];
                                             $nombre = $value2['nombrearbol'];
                                             $imgone = $value2['imgone'];
                                             $imgtwo = $value2['imgtwo'];
                                             $imgthree = $value2['imgthree'];
                                             $id = $value2['iden'];
                                        }
                                     }

                                ?>
                                    
                                    
                               <form action="logica/logadmin.php" method="post">     
                                <input type="hidden" name="iden" id="iden" placeholder=" identificacion " onfocus="this.placeholder = ''" onblur="this.placeholder = ' identificacion '" required="" value="<?php if(isset($id)){ echo $id;}?>" >
                                   
                                <input type="text" name="altura" id="altura" placeholder=" Altura " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Altura '" required="" value="<?php if(isset($altura)){ echo $altura;}?>" >

                                <input type="text" name="especie" id="especie" placeholder=" Nombre Del Árbol " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Nombre Del Árbol '" required=""  value="<?php if(isset($nombre)){ echo $nombre;}?>">
                            
                                <button type="submit" id="btnSave" >Save</button>
                                </form>
                            </div>
                            
                             
                            
                        </article>
                        <article id="setting">
                            
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
                              <div class="carousel-inner" style="height:220px; width:450px;  margin-left: 22%;">
                                <div class="carousel-item active" >
                                  <img class="d-block w-100" src="<?php if(isset($imgone)){ echo $imgone;}?>" alt="First slide" style="height:220px; width:450px;">
                                </div>
                                <div class="carousel-item">
                                  <img class="d-block w-100" src="<?php if(isset($imgtwo)){ echo $imgtwo;}?>" alt="Second slide" style="height:220px; width:450px;" >
                                </div>
                                <div class="carousel-item">
                                  <img class="d-block w-100" src="<?php if(isset($imgthree)){ echo $imgthree;}?>" alt="Third slide" style="height:220px; width:450px;">
                                </div>
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div>
                           
                        </article>
                       
                    </div>
           </div>
       </section>   
        
        
    </main>
  
         <!-- jQuery first, then Popper.js, then Bootstrap JS -->
         <script src="bootstraps/js/jquery-3.3.1.min.js"></script>
         <script src="bootstraps/js/bootstrap.min.js"></script>
        
    
     
        
    
    
    </body>
    
    
</html>